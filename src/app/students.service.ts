import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Student } from './interfaces/student';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

userCollection:AngularFirestoreCollection = this.db.collection('users');
studentsCollection:AngularFirestoreCollection;

getStudents(userId): Observable<any[]> {
  this.studentsCollection = this.db.collection(`users/${userId}/students`, 
     ref => ref.orderBy('time','desc').limit(10))
  return this.studentsCollection.snapshotChanges();    
} 



deleteStudent(userId:string, id:string){
  this.db.doc(`users/${userId}/students/${id}`).delete();
}

addStudent(userId:string,email:string, math:number, psico:number, selected:boolean,result:string,time:any){
  const student:Student = {math:math,email:email, psicometry:psico, selected:selected, result:result, time:time}
  this.userCollection.doc(userId).collection('students').add(student);
} 



constructor(private db: AngularFirestore,
  ) {} 

}
