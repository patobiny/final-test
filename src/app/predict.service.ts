import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  url = 'https://y6rgn7pyjh.execute-api.us-east-1.amazonaws.com/beta'; 
  
  predict(math:number, psicometry:number,selected:boolean):Observable<any>{
    let json = 
        {
          "math": math,
          "psicometry": psicometry,
          "selected":selected
        }
    
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res;       
      })
    );      
  }

  
  constructor(private http: HttpClient) { }
}
