import { StudentsService } from './../students.service';

import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { AuthService } from '../auth.service';

import { Student } from '../interfaces/student';
import { PredictService } from '../predict.service';
import { Router } from '@angular/router';
import { NodeWithI18n } from '@angular/compiler';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})

export class StudentFormComponent implements OnInit {

@Input() math:number;
@Input() psicometry:number;
@Input() payed:boolean;
@Input() formType:string;
@Output() update = new EventEmitter<Student>()
@Output() closeEdit = new EventEmitter<null>()

hasNotErrorMath:boolean=true;
hasNotErrorPsico:boolean=true;
hasNotErrorMathString:boolean=true;
hasNotErrorPsicoString:boolean=true;



students$;


prediction:string;
showDrop:boolean=false;
userId:string;
selected=false;

email;
time=Date.now();
predDrop:string;
options=["Will be ok","Will leave"];


 /**updateParent(){
  let student:Student = {id:this.id, math:this.math, psicometry:this.psicometry,payed:this.payed}
  this.update.emit(customer);
  if(this.formType=="Add customer"){
    this.name = null;
    this.years=null;
    this.income=null;

  };
 
}


tellparentToClose(){
  this.closeEdit.emit();
  
}

/* */
validatemath(math:number){
  if(math<0 || math>100){
    this.hasNotErrorMath=false;
    console.log(this.hasNotErrorMath);
  }
  if(Number.isInteger(math)==false){
    this.hasNotErrorMathString=false;
  };
}

validatepsico(psicometry:number){
  if(psicometry<0 || psicometry>800){
    this.hasNotErrorPsico=false;
    
  if(Number.isInteger(psicometry)==false){
    this.hasNotErrorPsicoString=false;  }
    console.log(this.hasNotErrorPsico);
  };
}



predict(math,psicometry,selected){
  console.log(math,psicometry,selected);
  this.predictService.predict(math,psicometry,selected).subscribe(
    res => {
      console.log(res);
    if(res>0.5){
      this.predDrop='Will be ok'
    }else{
      this.predDrop='Will leave'
    }return this.predDrop;
   }
    )

    }

   
    save(math,psicometry,selected,predDrop){
      this.studentsService.addStudent(this.userId,this.email,this.math, this.psicometry,this.selected,this.predDrop,this.time);
      this.router.navigate(['/students']);
    }
    

    cancel(){

    }
onSubmit(){}


  constructor(public predictService:PredictService,public authService:AuthService,private studentsService:StudentsService,private router:Router) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.email=user.email;
        this.students$ = this.studentsService.getStudents(this.userId);
        
    })}



  }

