import { PredictService } from './../predict.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { StudentsService } from '../students.service';
import { Student } from '../interfaces/student';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  name: string;
  completed: boolean;
 restocheck;
  userId;

  students:Student[];
  students$;
  addCustomerFormOpen=false;
  rowToEdit:number = -1; 

  list=[]; //for the checkboxxxxxxx
  isChecked=[]; ///for the checkboxxxxxxxxxx

 
  
  /**updateFromCheck(index){
    if(this.customers[index].result=='Will pay'){
      this.customers[index].result='Will default';
    }else{
      this.customers[index].result='Will pay';
    }
    this.customers[index].saved = true; 
    this.customersService.updateRsult(this.userId,this.customers[index].id,this.customers[index].result);

  }**/



  deleteCustomer(index){
    let id = this.students[index].id;
    this.studentsService.deleteStudent(this.userId, id);
  }

  


  displayedColumns: string[] = ['math', 'psicometry', 'payed','email','Delete', 'Result'];
 
  constructor(public studentsService:StudentsService,
    private authService:AuthService ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.students$ = this.studentsService.getStudents(this.userId);
          this.students$.subscribe(
            docs => {         
              this.students = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Student = document.payload.doc.data();
                
                customer.id = document.payload.doc.id;
                   this.students.push(customer); 
              }                        
            }
          )
      })
  }   
}
